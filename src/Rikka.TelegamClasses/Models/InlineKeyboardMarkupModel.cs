using Newtonsoft.Json;

namespace Rikka.TelegamClasses.Models
{

    
    public class InlineKeyboardMarkupModel
    {
        [JsonProperty(PropertyName="inline_keyboard")]
        public InlineKeyboardButtonModel[] InlineKeyboard { get; set; }
    }
}