﻿using System;

namespace Rikka.TelegramBotCore
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class BotActionAttribute : Attribute
    {
    }
}
