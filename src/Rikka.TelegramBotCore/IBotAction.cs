﻿using System.Threading.Tasks;
using Rikka.TelegamClasses.Models;
using Rikka.TelegramBotCore.Models;

namespace Rikka.TelegramBotCore
{
    public interface IBotAction
    {
        bool Private { get; }
        bool Chat { get; }
        string[] States { get; }
        string CommandName { get; }
        string Description { get; }
        Task<MessageFlow> Command(string command, MessageModel message);
        Task<MessageFlow> Message(string text, string state, MessageModel message);
    }
}
