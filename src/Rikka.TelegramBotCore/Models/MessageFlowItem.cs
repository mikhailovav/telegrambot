using System;

namespace Rikka.TelegramBotCore.Models
{
    public class MessageFlowItem
    {
        public MessageFlowItem(ISendItem sendItem, TimeSpan? span = null)
        {
            SendItem = sendItem;
            Span = span;
        }
        public MessageFlowItem(ISendItem sendItem, double seconds)
        {
            SendItem = sendItem;
            Span = TimeSpan.FromSeconds(seconds);
        }
        public static MessageFlowItem Message(string text, double seconds=0.3, int? chatId = null,object replyMarkup=null)
        {
            return new MessageFlowItem(new SendMessageModel(chatId, text, replyMarkup: replyMarkup), TimeSpan.FromSeconds(seconds));
        }
        public static MessageFlowItem Sticker(string sticker, double seconds = 0.3, int? chatId = null, object replyMarkup = null)
        {
            return new MessageFlowItem(new SendStickerModel(chatId, sticker, replyMarkup: replyMarkup), TimeSpan.FromSeconds(seconds));
        }
        public static MessageFlowItem Photo(byte[] bytes, double seconds = 0.3, int? chatId = null, object replyMarkup = null)
        {
            return new MessageFlowItem(new SendPhotoModel(chatId,bytes, replyMarkup: replyMarkup), TimeSpan.FromSeconds(seconds));
        }

        public ISendItem SendItem { get; set; }
        public TimeSpan? Span { get; set; }
    }
}