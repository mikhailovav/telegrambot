using System.Collections.Generic;

namespace Rikka.TelegramBotCore.Models
{
    public class MessageFlow:List<MessageFlowItem>
    {
        public MessageFlow()
        {

        }

        public MessageFlow(IEnumerable<MessageFlowItem> items) : base(items)
        {

        }

        public MessageFlow(MessageFlowItem item) : base(new[] { item })
        {

        }

        public MessageFlow(ISendItem item) : base(new[] { new MessageFlowItem(item) })
        {

        }


        public MessageFlow Message(string text, double seconds = 0.3, int? chatId = null, object replyMarkup = null)
        {
            this.Add(MessageFlowItem.Message(text,seconds,chatId,replyMarkup));
            return this;
        }
        public MessageFlow Sticker(string sticker, double seconds = 0.3, int? chatId = null, object replyMarkup = null)
        {
            this.Add(MessageFlowItem.Sticker(sticker, seconds, chatId, replyMarkup));
            return this;
        }
        public MessageFlow Photo(byte[] bytes, double seconds = 0.3, int? chatId = null, object replyMarkup = null)
        {
            this.Add(MessageFlowItem.Photo(bytes, seconds, chatId, replyMarkup));
            return this;
        }
    }
}